const exec = require('child_process').exec;
var fs = require('fs');
var cors = require('cors')
const express = require('express');
const upload = require('express-fileupload');
const app = express();

app.use(upload());
app.use(cors());

  app.post("/api/filecsv", async function(req,res){
    try {
      if(!req.files) {
        console.log("not uploaded");
        var data = {
          status: 400,
          message: 'No file uploaded',
          data: null
        };
        res.json(data);
      } else {
          //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
          let csv = req.files.csv;
          
          //Use the mv() method to place the file in upload directory (i.e. "uploads")
          csv.mv('./input_csv/' + csv.name);

          //EXECUTE ROI
          const childPorcess = await exec('java -jar G-RoI-0.0.1-SNAPSHOT-jar-with-dependencies.jar', function(err, stdout, stderr) {
            if (err) {
              console.log('Error');
                var data = {
                  "status": 400,
                  "error": "Execute Failed",
                  "response": null
                };
                res.json(data);              
            }
            else{
              console.log('Find ROI');
              console.log(stdout);
              
              const path = './input_csv/' + csv.name;
              if(fs.existsSync(path)){
                fs.unlinkSync(path);
                console.log("delete file " + csv.name);
              }

              var files = fs.readdirSync("output_json");
              var dir = 'output_json/'
              files.sort(function(a, b) {
                  return fs.statSync(dir + a).mtime.getTime() - fs.statSync(dir + b).mtime.getTime();
              });
              var arrayJSON = [];

              files.forEach(item => {
                arrayJSON.push(JSON.parse(fs.readFileSync('output_json/' + item, 'utf8')));
              });

              var data = {
                "status": 200,
                "error": null,
                "response": arrayJSON
              };
              res.json(data);
            }
          })
      }
    } catch (err) {
        res.status(500).send(err);
    }
  });

  app.get('/api/allroi', function (req, res) {
    var files = fs.readdirSync("output_json");
    files.sort(function(a, b) {
        return fs.statSync("output_json/" + a).mtime.getTime() - fs.statSync("output_json/" + b).mtime.getTime();
    });
    var arrayJSON = [];

    files.forEach(item => {
      arrayJSON.push(JSON.parse(fs.readFileSync('output_json/' + item, 'utf8')));
    });

    var data = {
      "status": 200,
      "error": null,
      "response": arrayJSON
    };
    res.json(data);

  });

  app.get('/api/json/:filename', function (req, res) {
    var dataJSON;

    if(fs.existsSync("./output_json/" + req.params.filename + ".json") == true){
      dataJSON = JSON.parse(fs.readFileSync('output_json/' + req.params.filename + ".json", 'utf8'))
    }
    else{
      console.log(req.params.filename + ".json not found")
    }

    var data = {
      "status": 200,
      "error": null,
      "response": dataJSON
    };
    res.json(data);

  });

  app.delete('/api/roi/:filename', function (req, res) {
    //await DeleteFile(req.params.filename);

    if(fs.existsSync("./output_json/" + req.params.filename + ".json") == true){
      fs.unlinkSync("./output_json/" + req.params.filename + ".json");
    }
    else{
      console.log(req.params.filename + ".json not found")
    }

    var files = fs.readdirSync("output_json");
    files.sort(function(a, b) {
        return fs.statSync("output_json/" + a).mtime.getTime() - fs.statSync("output_json/" + b).mtime.getTime();
    });
    var arrayJSON = [];

    files.forEach(item => {
      arrayJSON.push(JSON.parse(fs.readFileSync('output_json/' + item, 'utf8')));
    });

    var data = {
      "status": 200,
      "error": null,
      "response": arrayJSON
    };
    res.json(data);

  });

//async function DeleteFile(filename){
  //if(fs.existsSync("./output_json/" + filename + ".json") == true){
    //fs.unlinkSync("./output_json/" + filename + ".json");
  //}
  //else{
    //console.log(filename + ".json not found")
  //}
//}

app.listen(8000, () => {
    console.log('Server is running at port 8000');
});